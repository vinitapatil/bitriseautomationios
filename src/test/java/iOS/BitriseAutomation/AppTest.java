package iOS.BitriseAutomation;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
/**
 * Unit test for simple App.
 */
public class AppTest{
	
	public static AppiumDriver<MobileElement> driver;
	public DesiredCapabilities capabilities;

	@BeforeTest
	public void start() throws MalformedURLException{
		
		capabilities = new DesiredCapabilities();
		
		capabilities.setCapability("automationName", "XCUITest");					
		capabilities.setCapability("platformName", "iOS");
	    capabilities.setCapability("deviceName", "iPhone 6");
	    capabilities.setCapability("platformVersion", "10.3.3");
	    capabilities.setCapability("udid", "2e10c01c342b5feccd5b369b24451d322a6a0dc8");
		capabilities.setCapability("xcodeOrgId", "9YJJ8H3Q2");
		capabilities.setCapability("xcodeSigningId", "iPhone Developer");
		capabilities.setCapability("xcodeConfigfile","/Users/vipul/Documents/com.bookatable.AppiumWDA/com.bookatable.AppiumWDA/Config.xcconfig");
	    capabilities.setCapability("bundleId", "com.bookatable.apps.michelinrestaurants");

	    driver = new IOSDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
	    System.out.println("IOS IS RUNNING");
	    
	}
	
	@Test
	public void launchApp(){
		driver.findElement(By.xpath("//XCUIElementTypeOther[1]//XCUIElementTypeSearchField[1]")).sendKeys("LONDON");
		
	
	
}
}

